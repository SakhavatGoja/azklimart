$('.compare-swiper .swiper-slide .swiper-same-box .delete').on('click',function(){
  let $slide = $(this).parents('.swiper-slide')
  $slide.fadeOut(300, function(){
      $slide.remove();
  })
})

var swiper = new Swiper(".compared_boxes .swiper-container", {
  slidesPerGroup: 1,
  spaceBetween: 20,
  slidesOffsetAfter: 15,
  slidesOffsetBefore: 15,
  observer: true,
  breakpoints: {
    // when window width is >= 320px
    320: {
      slidesPerView: 2.2,
      spaceBetween: 8,
      slidesOffsetBefore: 5,
    },
    400: {
      slidesPerView: 2.5,
      spaceBetween: 8,
      slidesOffsetBefore: 0,
    },
    // when window width is >= 480px
    576: {
      slidesPerView: 1.8,
      slidesOffsetBefore: 0,
      spaceBetween: 15
    },
    991: {
      slidesPerView: 2.5,
    },
    // when window width is >= 640px
    1200: {
      slidesPerView: 3.2,
    }
  }
});

var indexswiper = new Swiper(".index-swiper .swiper-container", {
  pagination: {
    el: '.index-swiper .swiper-container .swiper-pagination',
    clickable: true
  },
  breakpoints: {
    320: {
      slidesPerView: 2.2,
      slidesPerGroup: 2,
      spaceBetween: 10,
      slidesOffsetAfter: 25
    },
    400: {
      slidesPerView: 2.2,
      slidesPerGroup: 2,
      spaceBetween: 20,
      slidesOffsetAfter: 25
    },
    576: {
      slidesPerView: 2.5,
      slidesPerGroup: 2,
      spaceBetween: 20
    },
    768: {
      slidesPerView: 3.3,
      slidesPerGroup: 3,
      spaceBetween: 20
    },
    991: {
      slidesPerView: 4,
      slidesPerGroup: 4,
      spaceBetween: 20
    }
  }
});

var comfortswiper = new Swiper(".brand-swiper .swiper-container", {
  spaceBetween: 13,
  slidesOffsetAfter: 0 ,
  pagination: {
    el: '.brand-swiper .swiper-container .swiper-pagination',
    clickable: true
  },
  breakpoints: {
    320: {
      slidesPerView: 2.2,
      slidesPerGroup: 2,
      slidesOffsetAfter: 25
    },
    400: {
      slidesPerView: 2.2,
      slidesPerGroup: 2,
      slidesOffsetAfter: 25
    },
    576: {
      slidesPerView: 3.1,
      slidesPerGroup: 3,
    },
    768: {
      slidesPerView: 4.4,
      slidesPerGroup: 4,
    },
    991: {
      slidesPerView: 5,
      slidesPerGroup: 5,
    }
  }
});


// special order
function swiperFlag(){
  if($(window).width() > 991){
    $('.special-order .swiper-container .swiper-wrapper').addClass("disabled");
    $('.special-order .swiper-container .swiper-pagination').addClass("disabled");
  }
  else{
    $('.special-order .swiper-container .swiper-wrapper').removeClass("disabled");
    $('.special-order .swiper-container .swiper-pagination').removeClass("disabled");
  }
}
$(window).on('load' ,function(){
  swiperFlag();
})

$(window).on('resize' ,function(){
  swiperFlag();
})

var specialOrder = new Swiper(".special-order .swiper-container", {
  pagination: {
    el: '.special-order .swiper-container .swiper-pagination',
    clickable: true
  },
  breakpoints: {
    320: {
      slidesPerView: 2.2,
      slidesPerGroup: 2,
      spaceBetween: 10,
      slidesOffsetAfter: 25
    },
    400: {
      slidesPerView: 2.2,
      slidesPerGroup: 2,
      spaceBetween: 20,
      slidesOffsetAfter: 25
    },
    576: {
      slidesPerView: 2.5,
      slidesPerGroup: 2,
      spaceBetween: 20
    },
    768: {
      slidesPerView: 3.3,
      slidesPerGroup: 3,
      spaceBetween: 20
    },
    991: {
      slidesPerView: 4,
      slidesPerGroup: 4,
      spaceBetween: 20
    }
  }
});

$('.landing_right .owl-carousel').owlCarousel({
  loop:false,
  margin:5,
  dots:true,
  items: 1,
  autoplay:true,
  autoplayTimeout:2000,
  loop:true,
})









