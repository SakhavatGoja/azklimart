AOS.init();

// basket-start
sum();

function sum(){
  var total = 0;
  $('.basket_operation .money_price').each(function(){
    total += +parseFloat($(this).text());
  });
    
  $(".result_box #total").text(total.toFixed(2));
}

var cartButtons = $('.basket_count_box').find('button');

$(cartButtons).on('click', function(e) {
  e.preventDefault();
  var $this = $(this);
  let $item = $(this).parents('.basket_operation').find('.money_price').find('span');
  let $itemPrice = parseFloat($item.data('price'))
  var target = $this.parent().data('target');
  var target = $('#' + target);
  var current = parseFloat($(target).val());
  if ($this.hasClass('cart-plus-1')){
      parseFloat(target.val(current + 1)).toFixed(2);
      $item.text(parseFloat($itemPrice * target.val()).toFixed(2))
      }
  else {
    if(current < 2){
      return null
    }
    else{
      target.val(current - 1);
      $item.text(parseFloat($itemPrice * target.val()).toFixed(2))
    }
  }

  sum()
  
});

$('.basket_count_box input').on('input', function(e) {
  var $this = $(this);
  let $item = $(this).parents('.basket_operation').find('.money_price').find('span');
  let $itemPrice = parseFloat($item.data('price'))
  var target = $this.parents('.basket_count_box').data('target');
  var target = $('#' + target);
  var current = parseFloat($(target).val());
  $item.text(parseFloat($itemPrice * target.val()).toFixed(2))
  sum();
});

$('.basket_same .delete').on('click',function(){
  let $parent = $(this).parents('.basket_same');
  $parent.fadeOut(300, function(){
       $parent.remove();
       sum();
  })
  if($('.basket_same').length < 2) setTimeout(() => {
    $('.result_box').remove()
  }, 300);
})

// basket-end


// tabs-start

$('.project_heading_container ul li button').on('click',function(){
  var data = $(this).data('tab');
  var div = $('.project_filtered_boxes .item_standart');
  $('.project_heading_container ul li button').removeClass('active_link');
  $(this).addClass('active_link');
  $('#loading').show();
	$('.projects_container').css('opacity','.1');
	setTimeout(function(){
		$('#loading').hide();
		$('.projects_container').css('opacity','unset');
	},600)
  if(!data){
      div.css('display','flex');
  }
  else{
      div.css('display','none');
      for(i=0;i<div.length;i++){
          var dataId = $(div[i]).data('id');
          if (data==dataId) $(div[i]).css('display','flex'); 
      }
  }    
})
// tabs-end

// filter-jquery-start
$('.f_spe_heading').on('click', function(){
  $(this).toggleClass('rotate')
  $(this).parents('.filter_special_box').find('.block').slideToggle()
})
// filter-jquery-end


// favourites-start
$('.box_favourite_same .delete').on('click',function(){
  let $parentFav = $(this).parents('.box_favourite_same')
  $parentFav.fadeOut(300, function(){
    $parentFav.remove();
  })
})
// favourites-end


// slider-range


// brand link
$('.brand_links a').on('click',function(e){
  e.preventDefault();
  $(this).toggleClass('active')
})
// brand link end

// thumb nail start
$('.image-area').thumbchanger({
  mainImageArea: '.main-image',
  subImageArea:  '.sub-image',
  animateTime:   600,
  easing:        'easeOutCubic',
  trigger:       'click',
});

$('.img-galery').on('click', function(e) {
  e.preventDefault()
    $('.imagepreview').attr('src', $(this).find('img').attr('src'));
    setTimeout(() => {
      $('#imagemodal').modal('show');
    }, 350);   
  });	
//  thumb nail end

// basket inner start
var innerBtn = $('.inner_count_box').find('button');

$(innerBtn).on('click', function(e) {
  e.preventDefault();
  var $this = $(this);
  let $item = $(this).parents('.inner-basket-area').find('span.price');
  let $itemPrice = parseInt($item.data('price'))
  var target = $this.parent().data('target');
  var target = $('#' + target);
  console.log(target)
  var current = parseFloat($(target).val());
  if ($this.hasClass('plus')){
        target.val(current + 1);
        $item.text($itemPrice * target.val())
      }
  else {
    if(current < 2){
      return null
    }
    else{
      target.val(current - 1);
      $item.text($itemPrice * target.val())
    }
  }
});

$('.inner_count_box input').on('input',function(){
  var $this = $(this);
  let $item = $(this).parents('.inner-basket-area').find('span.price');
  let $itemPrice = parseInt($item.data('price'))
  var target = $this.parents('.inner_count_box').data('target');
  var target = $('#' + target);
  var current = parseFloat($(target).val());
  $item.text($itemPrice * target.val())
})
// basket inner end


// input range slider filter start
$(".js-range-slider").ionRangeSlider({
    type: "double",
    min: 120,
    max: 7000,
    skin: "big",
    keyboard: true,
    force_edges: false
  }
)
let my_range = $(".js-range-slider").data("ionRangeSlider")
// input range slider filter end

// label filter start
$('.label_single input').on('change',function(){
  if($(this).is(':checked')){
    $(this).attr('checked' , 'true')
  }
  else{
    $(this).removeAttr('checked')
  }
})
// label filter end

// clean filter start
$('#clean').on('click', function(e){
  e.preventDefault();
  my_range.reset();
  $('.brand_links a').removeClass('active');
  $('.label_single input').each(function() {
    if($(this).attr('checked')){
        $(this).prop("checked", false).removeAttr('checked');
      }
  });
})
// clean filter end


// hamburger - start
$('.hamburger_link').on('click',function(e){
  e.preventDefault();
  $('.header_overlay').addClass('open');
  $('.overlay_loop').find('nav , .overlay_footer').show()
  $('.overlay_loop').find('form').hide()
  $('body').css('overflow', 'hidden');
})
// hamburger - end

// overlay  - start
$('.overlay_close').on('click', function(){
  $('.header_overlay').removeClass('open')
  setTimeout(() => {
    $('body').css('overflow', 'unset');
  }, 500);
}) 
// overlay - end

// search-mobile-start
$('.search_mobile').on('click',function(e){
  e.preventDefault();
  $('.header_overlay').addClass('open');
  $('.overlay_loop').find('nav , .overlay_footer').hide()
  $('.overlay_loop').find('form').show()
  $('body').css('overflow', 'hidden');
})
// search-mobile-end

// mobile-accordion-start
$('.list-unstyled .has-submenu-link button').on('click', function(){
  $(this).toggleClass('rotate');
  $(this).parents('.has-submenu-link').find('.submenu').slideToggle()
})
// mobile-accordion-end


// compare.btn start
$('.left_btns button.btn_blue').on('click',function(){
  $(this).parents('.compared_single_box').find('.compared_bottom').toggle(200)
})
// compare.btn end

// filter mobile start
$('button.filter').on('click', function(){
  $('.projects_main_box aside').addClass('open')
})
$('.projects_main_box aside button.esc').on('click',function(){
  $('.projects_main_box aside').removeClass('open')
})
$('body').on('click', function (e) {
  if ($('aside.aside-form').length !== 0
      && !$(e.target).hasClass('aside-form')       
      && !$(e.target).parents('.aside-form').length 
      && !$(e.target).hasClass('filter')
      && !$(e.target).parents('.filter').length
      ){
     $('.projects_main_box aside').removeClass('open')
  }
});
// filter mobile end

// new structur ul
$(".accordion_list li:first-child .submenu").slideDown()

$('.accordion_list li .link').click(function() {
  if ($(this).hasClass("active")) {
    $(this).removeClass("active").next().slideUp();
  } else {
    $(".accordion_list li .submenu").slideUp();
    $(".accordion_list li .link.active").removeClass("active");
    $(this).addClass("active").next().slideDown();
  }
  return false;
});

//         maxHeight = Math.max.apply(null, heights);
//  //Set #industries-slider1 height
//         $('#industries-slider1').height(maxHeight);