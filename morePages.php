<?php
    include("includes/head.php");
?>


<section class="more_items">
    <?php
        include("includes/header.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="box_repeat_container">
          <div class="breadcrumps">
            <div class="page_main">
              <a href="index.php" class="old_page">Azclimart</a>
            </div>
            <div class="breadcrump_img">
              <img src="img/breadcrump.svg" alt="">
            </div>
            <span class="current_page">Məhsullar</span>
          </div>
          <p class="title_repeat">Yeni məhsulları</p>
          
          <div class="box_parent">
            <div class="container_grid_4">
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <button class="see_more">Daha çox gör</button>
          </div>

          <div class="box_best_new">
            <div class="heading_best">
              <p class="title_repeat">Ən çox satan</p>
              <a href="#">Daha çox</a>
            </div>
            <div class="container_grid_4">
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                    <div class="discount_box">
                      <div class="month">12 ay</div>
                      <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                    <div class="discount_box">
                      <div class="month">12 ay</div>
                      <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                    <div class="discount_box">
                      <div class="month">12 ay</div>
                      <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                    <div class="discount_box">
                      <div class="month">12 ay</div>
                      <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
