<?php
    include("includes/head.php");
?>


<section class="more_items favourites">
    <?php
        include("includes/header.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="box_repeat_container">
          <div class="breadcrumps">
            <div class="page_main">
              <a href="index.php" class="old_page">Azclimart</a>
            </div>
            <div class="breadcrump_img">
              <img src="img/breadcrump.svg" alt="">
            </div>
            <span class="current_page">Favorilərim</span>
          </div>
          <p class="title_repeat">Favorilərim</p>
          <div class="box_parent">
            <div class="container_grid_4">
              <div class="box_favourite_same">
                <button class="delete">
                    <img src="img/esc.svg" alt="">
                </button>
                <div class="item_standart">
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                      <div class="discount_box">
                        <div class="month">12 ay</div>
                        <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                          <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M21.8332 11.0001C21.8332 5.02008 16.9798 0.166748 10.9998 0.166748C5.01984 0.166748 0.166504 5.02008 0.166504 11.0001C0.166504 16.9801 5.01984 21.8334 10.9998 21.8334C16.9798 21.8334 21.8332 16.9801 21.8332 11.0001ZM14.2498 5.04175L18.0415 8.83342L14.2498 12.6251V9.91675H9.9165V7.75008H14.2498V5.04175ZM7.74984 16.9584L3.95817 13.1667L7.74984 9.37508V12.0834H12.0832V14.2501H7.74984V16.9584Z" fill="#2D4587"/>
                          </svg>
                      </a>
                      <a href="#" class="active">
                          <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M10.9998 20.1292L9.429 18.6992C3.84984 13.64 0.166504 10.3033 0.166504 6.20833C0.166504 2.87167 2.78817 0.25 6.12484 0.25C8.00984 0.25 9.819 1.1275 10.9998 2.51417C12.1807 1.1275 13.9898 0.25 15.8748 0.25C19.2115 0.25 21.8332 2.87167 21.8332 6.20833C21.8332 10.3033 18.1498 13.64 12.5707 18.71L10.9998 20.1292Z" fill="#FF304F"/>
                          </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box_favourite_same">
                <button class="delete">
                    <img src="img/esc.svg" alt="">
                </button>
                <div class="item_standart">
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                      <div class="discount_box">
                        <div class="month">12 ay</div>
                        <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                          <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M21.8332 11.0001C21.8332 5.02008 16.9798 0.166748 10.9998 0.166748C5.01984 0.166748 0.166504 5.02008 0.166504 11.0001C0.166504 16.9801 5.01984 21.8334 10.9998 21.8334C16.9798 21.8334 21.8332 16.9801 21.8332 11.0001ZM14.2498 5.04175L18.0415 8.83342L14.2498 12.6251V9.91675H9.9165V7.75008H14.2498V5.04175ZM7.74984 16.9584L3.95817 13.1667L7.74984 9.37508V12.0834H12.0832V14.2501H7.74984V16.9584Z" fill="#2D4587"/>
                          </svg>
                      </a>
                      <a href="#" class="active">
                          <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M10.9998 20.1292L9.429 18.6992C3.84984 13.64 0.166504 10.3033 0.166504 6.20833C0.166504 2.87167 2.78817 0.25 6.12484 0.25C8.00984 0.25 9.819 1.1275 10.9998 2.51417C12.1807 1.1275 13.9898 0.25 15.8748 0.25C19.2115 0.25 21.8332 2.87167 21.8332 6.20833C21.8332 10.3033 18.1498 13.64 12.5707 18.71L10.9998 20.1292Z" fill="#FF304F"/>
                          </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box_favourite_same">
                <button class="delete">
                    <img src="img/esc.svg" alt="">
                </button>
                <div class="item_standart">
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                      <div class="discount_box">
                        <div class="month">12 ay</div>
                        <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                          <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M21.8332 11.0001C21.8332 5.02008 16.9798 0.166748 10.9998 0.166748C5.01984 0.166748 0.166504 5.02008 0.166504 11.0001C0.166504 16.9801 5.01984 21.8334 10.9998 21.8334C16.9798 21.8334 21.8332 16.9801 21.8332 11.0001ZM14.2498 5.04175L18.0415 8.83342L14.2498 12.6251V9.91675H9.9165V7.75008H14.2498V5.04175ZM7.74984 16.9584L3.95817 13.1667L7.74984 9.37508V12.0834H12.0832V14.2501H7.74984V16.9584Z" fill="#2D4587"/>
                          </svg>
                      </a>
                      <a href="#" class="active">
                          <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M10.9998 20.1292L9.429 18.6992C3.84984 13.64 0.166504 10.3033 0.166504 6.20833C0.166504 2.87167 2.78817 0.25 6.12484 0.25C8.00984 0.25 9.819 1.1275 10.9998 2.51417C12.1807 1.1275 13.9898 0.25 15.8748 0.25C19.2115 0.25 21.8332 2.87167 21.8332 6.20833C21.8332 10.3033 18.1498 13.64 12.5707 18.71L10.9998 20.1292Z" fill="#FF304F"/>
                          </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box_favourite_same">
                <button class="delete">
                    <img src="img/esc.svg" alt="">
                </button>
                <div class="item_standart">
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                      <div class="discount_box">
                        <div class="month">12 ay</div>
                        <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                          <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M21.8332 11.0001C21.8332 5.02008 16.9798 0.166748 10.9998 0.166748C5.01984 0.166748 0.166504 5.02008 0.166504 11.0001C0.166504 16.9801 5.01984 21.8334 10.9998 21.8334C16.9798 21.8334 21.8332 16.9801 21.8332 11.0001ZM14.2498 5.04175L18.0415 8.83342L14.2498 12.6251V9.91675H9.9165V7.75008H14.2498V5.04175ZM7.74984 16.9584L3.95817 13.1667L7.74984 9.37508V12.0834H12.0832V14.2501H7.74984V16.9584Z" fill="#2D4587"/>
                          </svg>
                      </a>
                      <a href="#" class="active">
                          <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M10.9998 20.1292L9.429 18.6992C3.84984 13.64 0.166504 10.3033 0.166504 6.20833C0.166504 2.87167 2.78817 0.25 6.12484 0.25C8.00984 0.25 9.819 1.1275 10.9998 2.51417C12.1807 1.1275 13.9898 0.25 15.8748 0.25C19.2115 0.25 21.8332 2.87167 21.8332 6.20833C21.8332 10.3033 18.1498 13.64 12.5707 18.71L10.9998 20.1292Z" fill="#FF304F"/>
                          </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box_favourite_same">
                <button class="delete">
                    <img src="img/esc.svg" alt="">
                </button>
                <div class="item_standart">
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                      <div class="discount_box">
                        <div class="month">12 ay</div>
                        <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                          <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M21.8332 11.0001C21.8332 5.02008 16.9798 0.166748 10.9998 0.166748C5.01984 0.166748 0.166504 5.02008 0.166504 11.0001C0.166504 16.9801 5.01984 21.8334 10.9998 21.8334C16.9798 21.8334 21.8332 16.9801 21.8332 11.0001ZM14.2498 5.04175L18.0415 8.83342L14.2498 12.6251V9.91675H9.9165V7.75008H14.2498V5.04175ZM7.74984 16.9584L3.95817 13.1667L7.74984 9.37508V12.0834H12.0832V14.2501H7.74984V16.9584Z" fill="#2D4587"/>
                          </svg>
                      </a>
                      <a href="#" class="active">
                          <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M10.9998 20.1292L9.429 18.6992C3.84984 13.64 0.166504 10.3033 0.166504 6.20833C0.166504 2.87167 2.78817 0.25 6.12484 0.25C8.00984 0.25 9.819 1.1275 10.9998 2.51417C12.1807 1.1275 13.9898 0.25 15.8748 0.25C19.2115 0.25 21.8332 2.87167 21.8332 6.20833C21.8332 10.3033 18.1498 13.64 12.5707 18.71L10.9998 20.1292Z" fill="#FF304F"/>
                          </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
