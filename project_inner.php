<?php
    include("includes/head.php");
?>
<section class="project_inner">
  <?php
    include("includes/header.php");
    ?>
  <div class="project_inner_container">
    <div class="container">
      <div class="row">
        <div class="breadcrumps">
          <div class="page_main">
            <a href="index.php" class="old_page">Azclimart</a>
          </div>
          <div class="breadcrump_img">
            <img src="img/breadcrump.svg" alt="">
          </div>
          <div class="page_main">
            <a href="index.php" class="old_page">Məhsullar</a>
          </div>
          <div class="breadcrump_img">
            <img src="img/breadcrump.svg" alt="">
          </div>
          <span class="current_page">Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR</span>
        </div>
        <div class="inner_top ">
          <div class="image-area clearfix">
            <div class="main-image">
              <div class="img-galery"><img src="https://unsplash.it/400/200?image=885" /></div>
              <div class="img-galery"><img src="https://unsplash.it/400/200?image=882" /></div>
              <div class="img-galery"><img src="https://unsplash.it/400/200?image=883" /></div>
            </div>
            <div class="sub-image">
              <div><img src="https://unsplash.it/123/80?image=885" alt="" /></div>
              <div><img src="https://unsplash.it/123/80?image=882" alt="" /></div>
              <div><img src="https://unsplash.it/123/80?image=883" alt="" /></div>
            </div>
          </div>
          <div class="inner-basket-area">
            <p class="title-inner">Kondisioner</p>
            <p class="name-model">
              Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR
            </p>
            <p class="code-model">Məhsulun kodu : <span>12343526181872</span></p>
            <p class="content-model">
              Qeyd: Kondisionerin təsir sahəsi bu xüsusiyyətlərə görə dəyişə bilər: Tavanın hündürlüyü, günəş işığının düşmə 
              bucağı, otaqda olan sakinlərin sayı,mütəmadi istifadə olunan avadanlıqların ( televizor, notbuk, soyuducu və.s ) sayı,
              pəncərənin ölçüləri və s.
            </p>
            <div class="inner-basket-operation">
              <p><span class="price" data-price="3200">3200</span>₼</p>
              <div class="inner-operation">
                <a href="basket.php" class="add_basket_inner blue-white">Səbətə əlavə et</a>
                <div class="inner_count_box" data-target="inner_price">
                  <button class="minus">-</button>
                  <input type="number" id="inner_price" value="1" name="" min="1">
                  <button class="plus">+</button>
                </div>
                <div class="opeation_socials">
                  <a href="compare.php"><img src="img/compare_blue.svg" alt=""></a>
                  <a href="favoutires.php"><img src="img/fav_blue.svg" alt=""></a>
                </div>
              </div>
            </div>
            <div class="inner-social">
              <p>Paylaş</p>
              <div class="socials_box">
                <a href="#"><img src="img/inner_fb.jpg" alt=""></a>
                <a href="#"><img src="img/inner_lnk.jpg" alt=""></a>
                <a href="#"><img src="img/inner_tg.jpg" alt=""></a>
                <a href="#"><img src="img/inner_tw.jpg" alt=""></a>
                <a href="#"><img src="img/inner_wp.jpg" alt=""></a>
                <a href="#"><img src="img/inner_mail.jpg" alt=""></a>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="main_1400">
      <div class="container">
        <div class="row">
          <div class="accordion_box">
            <ul class="accordion">
              <li class="open">
                <div class="link">
                    <p class="link_accordion">Məhsul haqqında məlumat</p>
                    <button><img src="img/plus.svg" alt=""></button>
                </div>
                <div class="submenu">
                    <div class="submenu_table">
                      <div class="sub_row">
                        <p>Təyinat tipi</p>
                        <p>Multi sistem 1 bloklu</p>
                      </div>
                      <div class="sub_row">
                        <p>BTU</p>
                        <p>3600</p>
                      </div>
                      <div class="sub_row">
                        <p>Daxili blokların sayı</p>
                        <p>5</p>
                      </div>
                      <div class="sub_row">
                        <p>Knverter</p>
                        <p>Bəli</p>
                      </div>
                      <div class="sub_row">
                        <p>Freon</p>
                        <p>R32</p>
                      </div>
                    </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="kon_abs_img">
        <img src="img/kondisioner.png" alt="">
      </div>
    </div>
    
    <div class="container">
      <div class="row">
        <div class="box_fraction_about w-100">
          <div class="single_about_box">
            <div><img src="img/guarantee.svg" alt=""></div>
            <p>Məhsula zəmanət</p>
          </div>
          <div class="single_about_box">
            <div><img src="img/delivery.svg" alt=""></div>
            <p>Pulsuz çatdırılma</p>
          </div>
          <div class="single_about_box">
            <div><img src="img/advice.svg" alt=""></div>
            <p>Mühendis mesleheti</p>
          </div>
        </div>
        <div class="box_best_new">
          <div class="heading_best">
            <p class="title_repeat">Ən çox satan</p>
            <a href="#">Daha çox</a>
          </div>
          <div class="container_grid_4">
            <div class="item_standart">
              <a href="#" class="item_img">
                <div class="dis_type">-15%</div>
                <img src="img/product.jpg" alt="">
              </a>
              <div class="item_info">
                <div class="item_price_box">
                  <div class="prices">
                    <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                    <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                  </div>
                  <div class="discount_box">
                    <div class="month">12 ay</div>
                    <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                  </div>
                </div>
                <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                <div class="item_controller">
                  <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                  <a href="#">
                    <img src="img/compare_gray.svg" class="item_gray" alt="">
                    <img src="img/compare_blue.svg" class="item_blue" alt="">
                  </a>
                  <a href="#">
                    <img src="img/fav_gray.svg" class="item_gray" alt="">
                    <img src="img/fav_blue.svg" class="item_blue" alt="">
                  </a>
                </div>
              </div>
            </div>
            <div class="item_standart">
              <a href="#" class="item_img">
                <div class="dis_type">-15%</div>
                <img src="img/product.jpg" alt="">
              </a>
              <div class="item_info">
                <div class="item_price_box">
                  <div class="prices">
                    <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                    <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                  </div>
                  <div class="discount_box">
                    <div class="month">12 ay</div>
                    <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                  </div>
                </div>
                <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                <div class="item_controller">
                  <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                  <a href="#">
                    <img src="img/compare_gray.svg" class="item_gray" alt="">
                    <img src="img/compare_blue.svg" class="item_blue" alt="">
                  </a>
                  <a href="#">
                    <img src="img/fav_gray.svg" class="item_gray" alt="">
                    <img src="img/fav_blue.svg" class="item_blue" alt="">
                  </a>
                </div>
              </div>
            </div>
            <div class="item_standart">
              <a href="#" class="item_img">
                <div class="dis_type">-15%</div>
                <img src="img/product.jpg" alt="">
              </a>
              <div class="item_info">
                <div class="item_price_box">
                  <div class="prices">
                    <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                    <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                  </div>
                  <div class="discount_box">
                    <div class="month">12 ay</div>
                    <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                  </div>
                </div>
                <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                <div class="item_controller">
                  <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                  <a href="#">
                    <img src="img/compare_gray.svg" class="item_gray" alt="">
                    <img src="img/compare_blue.svg" class="item_blue" alt="">
                  </a>
                  <a href="#">
                    <img src="img/fav_gray.svg" class="item_gray" alt="">
                    <img src="img/fav_blue.svg" class="item_blue" alt="">
                  </a>
                </div>
              </div>
            </div>
            <div class="item_standart">
              <a href="#" class="item_img">
                <div class="dis_type">-15%</div>
                <img src="img/product.jpg" alt="">
              </a>
              <div class="item_info">
                <div class="item_price_box">
                  <div class="prices">
                    <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                    <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                  </div>
                  <div class="discount_box">
                    <div class="month">12 ay</div>
                    <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                  </div>
                </div>
                <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                <div class="item_controller">
                  <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                  <a href="#">
                    <img src="img/compare_gray.svg" class="item_gray" alt="">
                    <img src="img/compare_blue.svg" class="item_blue" alt="">
                  </a>
                  <a href="#">
                    <img src="img/fav_gray.svg" class="item_gray" alt="">
                    <img src="img/fav_blue.svg" class="item_blue" alt="">
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      
  </div>

  <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">              
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal"><img src="img/esc.svg" alt=""></button>
          <img src="" class="imagepreview w-100" >
        </div>
      </div>
    </div>
  </div>

  <?php
    include("includes/footer.php");
  ?>
</section>
<script>
  	
</script>

<?php
    include("includes/script.php");
?>