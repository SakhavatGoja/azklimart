<footer>
  <div class="footer_container">
    <div class="container">
      <div class="row">
        <div class="footer_main w-100">
          <div class="footer_left">
            <a href="#"><img src="img/footer_logo.svg" alt=""></a>
            <p>Azclimart şirkəti kondisioner, havalandırıcı, santexnika bölmələrinə aid məhsul satan şirkətdir.</p>
          </div>
          <div class="footer_right">
            <div class="footer">
              <ul>
                <li><a href="#">Məhsullar</a></li>
                <li><a onclick="selectNews(1)" href="projects.php">Kondisioner bölümü </a></li>
                <li><a onclick="selectNews(2)" href="projects.php">Havalandırma bölümü</a></li>
                <li><a onclick="selectNews(3)" href="projects.php">Santexnika bölümü</a></li>
                <li><a href="#">Haqqimizda</a></li>
                <li><a href="fag.php">FAG</a></li>
                <li><a href="#">Əlaqə</a></li>
                <li><a href="#"><div><img src="img/f_phone.svg" alt=""></div> +99451 206 53 66</a></li>
                <li><a href="#"><div><img src="img/f_loc.svg" alt=""></div> Bakı ş. Nərimanov ray. Ziya</a></li>
                <li><a href="#"><div><img src="img/f_mail.svg" alt=""></div> info@katsangroup.az</a></li>
              </ul>
            </div>
            <div class="footer_socials">
              <a href="#"><p>Site by</p><img src="img/bcp.svg" alt=""></a>
              <a href="#"><img src="img/fb.svg" alt=""></a>
              <a href="#"><img src="img/insta.svg" alt=""></a>
            </div>
          </div>
        </div>
        <div class="footer_bottom"><p>Bütün hüquqlar qorunur © 2019 </p><a href="index.php">Azclimart</a></div>
      </div>
    </div>
  </div>
</footer>