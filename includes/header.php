<header>
  <div class="w-100 header_top_container">
    <div class="container">
      <div class="row">
        <div class="header_top_box w-100">
          <a href="#" class="number_box">
            <img src="img/phone_header.svg" alt="">
            <p>(+99412) 567 16 06</p>
          </a>
          <div class="languages_box">
            <a href="#">Ru</a>
            <a href="#">En</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="w-100 header_bottom_container">
    <div class="container">
      <div class="row">
        <div class="header_bottom_box w-100">
          <a href="index.php" class="logo"><img src="img/logo.svg" alt=""></a>
          <div class="header_main_box">
            <form action="">
              <input type="text" placeholder="Axtar">
              <button type="submit">
                <img src="img/search.svg" alt="">
              </button>
            </form>
            <ul>
              <li>
                <a href="projects.php">Məhsullar</a>
                <div class="has-submenu">
                  <ul class="submenu_links">
                    <li>
                      <a href="#">
                        Kondisioner bölümü
                      </a>
                      <div class="submenu_multiple_has_menu">
                        <div class="multiple_submenu_links">
                          <a href="#">AHU-lar</a>
                          <a href="#">Rekuperatorlar</a>
                          <a href="#">Hava Kaloriferlər</a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <a href="#">
                        Kondisioner bölümü
                      </a>
                      <div class="submenu_multiple_has_menu">
                        <div class="multiple_submenu_links">
                          <a href="#">AHU-lar</a>
                          <a href="#">Rekuperatorlar</a>
                          <a href="#">Fanlar</a>
                          <a href="#">Hava kanalları</a>
                          <a href="#">Flexlər</a>
                          <a href="#">Fanlar</a>
                          <a href="#">Hava Kaloriferlər</a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <a href="#">
                        Kondisioner bölümü
                      </a>
                      <div class="submenu_multiple_has_menu">
                        <div class="multiple_submenu_links">
                          <a href="#">Flexlər</a>
                          <a href="#">Fanlar</a>
                          <a href="#">Hava Kaloriferlər</a>
                        </div>
                      </div>
                    </li>
                    
                  </ul>
                </div>
              </li>
              <li><a href="about.php">Haqqimizda</a></li>
              <li><a href="contact.php">Əlaqə</a></li>
              <li>
                  <a href="compare.php">
                    <img src="img/compare.svg" class="white_class" alt="">
                    <img src="img/compare_blue.svg" class="blue_class" alt="">
                  </a>
              </li>
              <li>
                  <a href="favourites.php">
                    <img src="img/fav.svg" class="white_class" alt="">
                    <img src="img/fav_blue.svg" class="blue_class" alt="">
                  </a>
              </li>
              <li>
                  <a href="basket.php">
                    <img src="img/basket.svg" class="white_class" alt="">
                    <img src="img/header_basket_blue.svg" class="blue_class" alt="">
                  </a>
              </li>
            </ul>
          </div>
          <div class="header_mobile_box">
            <a href="#" class="search_mobile"><img src="img/search.svg" alt=""></a>
            <a href="basket.php"><img src="img/basket_blue.svg" alt=""></a>
            <a href="#" class="hamburger_link"><img src="img/hamburger.svg" alt=""></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>

<div class="header_overlay">
  <div class="overlay_loop">
    <div class="overlay_heading">
      <a href="#"><img src="img/footer_logo.svg" alt=""></a>
      <button class="overlay_close"><img src="img/esc.svg" alt=""></button>
    </div>

    <nav>
      <ul class="list-unstyled">
        <li><a href="projects.php">Məhsullar</a></li>
        <li class="has-submenu-link">
          <div><a href="#">Kondisioner bölümü</a><button><button><img src="img/mobile-arrow.svg" alt=""></button></button></div>
          <div class="submenu">
            <ul>
              <li><a href="#">AHU-lar</a></li>
              <li><a href="#">Rekuperatorlar</a></li>
              <li><a href="#">Fanlar</a></li>
              <li><a href="#">Hava kanalları</a></li>
              <li><a href="#">Flexlər</a></li>
              <li><a href="#">Kaloriferlər</a></li>
              <li><a href="#">Mənfəzlər</a></li>
            </ul>
          </div>
        </li>
        <li class="has-submenu-link">
          <div><a href="#">Havalandırma bölümü</a><button><img src="img/mobile-arrow.svg" alt=""></button></div>
          <div class="submenu">
            <ul>
              <li><a href="#">AHU-lar</a></li>
              <li><a href="#">Rekuperatorlar</a></li>
              <li><a href="#">Fanlar</a></li>
              <li><a href="#">Hava kanalları</a></li>
              <li><a href="#">Flexlər</a></li>
              <li><a href="#">Kaloriferlər</a></li>
              <li><a href="#">Mənfəzlər</a></li>
            </ul>
          </div>
        </li>
        <li class="has-submenu-link">
          <div><a href="#">Santexnika bölümü</a><button><img src="img/mobile-arrow.svg" alt=""></button></div>
          <div class="submenu">
            <ul>
              <li><a href="#">AHU-lar</a></li>
              <li><a href="#">Rekuperatorlar</a></li>
              <li><a href="#">Fanlar</a></li>
              <li><a href="#">Hava kanalları</a></li>
              <li><a href="#">Flexlər</a></li>
              <li><a href="#">Kaloriferlər</a></li>
              <li><a href="#">Mənfəzlər</a></li>
            </ul>
          </div>
        </li>
        <li><a href="#">Haqqımızda</a></li>
        <li><a href="#">Əlaqə</a></li>
      </ul>
    </nav>

    <div class="overlay_footer">
      <div class="over_footer_favs">
        <a href="compare.php"><img src="img/compare.svg" alt=""></a>
        <a href="favourites.php"><img src="img/fav.svg" alt=""></a>
      </div>
      <div class="over_footer_langs">
        <a href="#">Ru</a>
        <a href="#">En</a>
      </div>
    </div>

    <form action="" class="form-mobile">
      <input type="text" placeholder="Axtar">
      <button type="submit"><img src="img/search.svg" alt=""></button>
    </form>
  </div>
</div>

