<?php
    include("includes/head.php");
?>


<section class="pay">
    <?php
        include("includes/header.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="pay_container">
          <div class="breadcrumps">
            <div class="page_main">
              <a href="index.php" class="old_page">Azclimart</a>
            </div>
            <div class="breadcrump_img">
              <img src="img/breadcrump.svg" alt="">
            </div>
            <span class="current_page">Sifarişi tamamla</span>
          </div>
          <p class="title_repeat">Sifarişi tamamla</p>
          <form action="" id="pay_form">
            <div class="contact_form_container">
              <div class="fraction-form">
                <p class="form_title">Əlaqə məlumatları</p>
                <div class="contact_form_box">
                  <div class="contact_left">
                    <div class="form-group" >
                      <input class="inputValidate" type="text" name="name" required>
                      <label class="place-label">Ad <span>*</span></label>
                    </div>
                    <div class="form-group" >
                      <input class="inputValidate" type="text" name="surname" required>
                      <label class="place-label">Soyad <span>*</span></label>
                    </div>
                    <div class="form-group" >
                      <input class="inputValidate" type="text" name="father_name" required>
                      <label class="place-label">Ata adı <span>*</span></label>
                    </div>
                    <div class="form-group" >
                      <input class="inputValidate number_input" type="number" minlength="10" name="phone" required>
                      <label class="place-label">Telefon nömrəsi <span>*</span></label>
                    </div>
                    <button class="add_new_field"><img src="img/add.jpg" alt=""> Əlavə telefon nömrəsi</button>
                  </div>
                  <div class="contact_right">
                    <div class="form-group" >
                      <input class="inputValidate" type="email" name="email" required>
                      <label class="place-label">Email <span>*</span></label>
                    </div>
                    <div class="form-group">
                      <textarea name="textarea"></textarea>
                      <label class="place-label">Əlavə qeyd </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="fraction-form">
                <p class="form_title">Çatdırılma ünvanı</p>
                <div class="contact_form_box">
                  <div class="form-group">
                    <select name="city[]" class="nice-select inputValidate" required>
                        <option value= "" disabled selected> Şəhər</option>
                        <option value="1">Bakı</option>
                        <option value="2">Sumqayıt</option>
                        <option value="3">Gəncə</option>
                        <option value="4">Mingəçevir</option>
                    </select>
                  </div>
                  <div class="form-group" >
                      <input class="inputValidate" type="text" name="address" required>
                      <label class="place-label">Ünvan <span>*</span></label>
                    </div>
                </div>
              </div>
              <div class="fraction-form radio-fraction">
                <p class="form_title">Ödəmə üsulu</p>
                <div class="contact_form_box">
                  <div class="form-group">

                    <div class="form-label">
                      <input type="radio" name="radioname" id="cash" />
                      <label for="cash" class="label-main">
                        <div class="label-img"><img src="img/cash.svg" alt=""></div>
                        <p>Çatdırıldıqda nağd ödə</p>
                      </label>
                    </div>

                    <div class="form-label">
                      <input type="radio" name="radioname" id="online" />
                      <label for="online" class="label-main">
                        <div class="label-img"><img src="img/online.svg" alt=""></div>
                        <p>Online ödə</p>
                      </label>
                    </div>

                    <div class="form-label">
                      <input type="radio" name="radioname" id="cart" />
                      <label for="cart" class="label-main">
                        <div class="label-img"><img src="img/cart.svg" alt=""></div>
                        <p>Çatdırıldıqda kartı ilə ödə</p>
                      </label>
                    </div>

                    <div class="form-label">
                      <input type="radio" name="radioname" id="bircart" />
                      <label for="bircart" class="label-main">
                        <div class="label-img"><img src="img/birkart.svg" alt=""></div>
                        <p>Birkatla ödə</p>
                      </label>
                    </div>
                    
                     
                  </div>
                </div>
              </div>
            </div>
            <div class="form_result_box">
              <p class="title_result_form">Sifarişin qiyməti</p>
              <div class="info_form_container">
                <div class="count_form_box">
                  <p class="count_product"><span>2</span>məhsul</p>
                  <p class="price_product same_result_text">
                    <span>7000</span><img src="img/manatBlue.jpg" alt="">
                  </p>
                </div>
                <div class="delivery_form_box">
                  <p class="title_delivery">Çatdırılma</p>
                  <p class="delivery_price same_result_text">PULSUZ</p>
                </div>
              </div>
              <div class="result_form_container">
                <div class="box_summary">
                  <p class="title_summary">Ümumi ödəniləcək</p>
                  <p class="price_summary">7000<img src="img/manat.svg" alt=""></p>
                </div>
                <button type="submit" class="btn_blue">tamamla</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="modal fade">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal"><img src="img/esc.svg" alt=""></button>
          <div class="success_content content_flex">
            <div class="status_img"><img src="img/success.jpg" alt=""></div>
            <p class="modal_title">Sifarişiniz uğurla tamamlandı.</p>
            <p class="modal_context">Təsdiqləndikdən sonra  elanınız səhifədə yerləşdiriləcək.</p>
          </div>
          <div class="unsuccess_content content_flex">
            <div class="status_img"><img src="img/unsuccess.jpg" alt=""></div>
            <p class="modal_title">Sifarişiniz uğurla tamamlandı.</p>
            <p class="modal_context">Təsdiqləndikdən sonra  elanınız səhifədə yerləşdiriləcək.</p>
          </div>
        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
