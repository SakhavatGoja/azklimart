<?php
    include("includes/head.php");
?>
<section class="about">
    <?php
      include("includes/header.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="about_container w-100">
          <div class="breadcrumps">
            <div class="page_main">
              <a href="index.php" class="old_page">Azclimart</a>
            </div>
            <div class="breadcrump_img">
              <img src="img/breadcrump.svg" alt="">
            </div>
            <span class="current_page">Haqqında</span>
          </div>
          <div class="about_top">
            <div class="about_img">
              <img src="img/about.png" alt="">
            </div>
            <div class="about_top_content">
              <p class="title_about">Rahatlığınızı bize etibar edin<span>“</span></p>
              <p class="content_about">
                “AZKLİMART” MMC şirkəti yarandığı gündən etibarən bir çox uğurlu layihələrə imza atmışdır. Şirkət, hər növ inzibati, yaşayış, istehsalat və iaşə obyektlərində isitmə, soyutma, havalandırma işlərinin layihələndirilməsi və montajını həyata keçirir. Həmçinin yanğınsöndürmə, suyun filtirasiyası və isti-soyuq su təchizatı işlərinin görülməsi də şirkətimizin tərəfindən yüksək 
                səviyyədə həyata keçirilir.
                </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="about_bottom_container">
      <div class="custom-shape">
        <!-- <svg width="100%" height="100%" id="svg" viewBox="0 0 1440 500" xmlns="http://www.w3.org/2000/svg" class="transition duration-300 ease-in-out delay-150"><defs><linearGradient id="gradient" x1="50%" y1="0%" x2="50%" y2="100%"><stop offset="0" stop-color="#ffffffff"></stop><stop offset="83%" stop-color="rgba(255, 255, 255, 0.83)"></stop></linearGradient></defs><path d="M 0,500 C 0,500 0,250 0,250 C 182.2666666666667,289.2 364.5333333333334,328.4 531,309 C 697.4666666666666,289.6 848.1333333333334,211.60000000000002 997,192 C 1145.8666666666666,172.39999999999998 1292.9333333333334,211.2 1440,250 C 1440,250 1440,500 1440,500 Z" stroke="none" stroke-width="0" fill="url(#gradient)" class="transition-all duration-300 ease-in-out delay-150 path-0"></path></svg> -->
      </div>
      <div class="container">
        <div class="row">
          <div class="about_three_box w-100">
            <div class="box_fraction_about">
              <div class="single_about_box">
                <div><img src="img/guarantee.svg" alt=""></div>
                <p>Məhsula zəmanət</p>
              </div>
              <div class="single_about_box">
                <div><img src="img/delivery.svg" alt=""></div>
                <p>Pulsuz çatdırılma</p>
              </div>
              <div class="single_about_box">
                <div><img src="img/advice.svg" alt=""></div>
                <p>Mühendis mesleheti</p>
              </div>
            </div>
          </div>
          <p class="content_about">
            “AZKLİMART” MMC şirkəti yarandığı gündən etibarən bir çox uğurlu layihələrə imza atmışdır.
             Şirkət, hər növ inzibati, yaşayış, istehsalat və iaşə obyektlərində isitmə, soyutma, havalandırma işlərinin 
             layihələndirilməsi və montajını həyata keçirir. Həmçinin yanğınsöndürmə, suyun filtirasiyası və isti-soyuq su təchizatı 
             işlərinin görülməsi də şirkətimizin tərəfindən yüksək səviyyədə həyata keçirilir.
          </p>
        </div>
      </div>
    </div>
    <?php
      include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>