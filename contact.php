<?php
    include("includes/head.php");
?>
<section class="contact">
    <?php
      include("includes/header.php");
    ?>
    <div class="position_absolute"></div>
    <div class="container">
      <div class="row">
        <div class="contact_container w-100">
          <div class="breadcrumps">
            <div class="page_main">
              <a href="index.php" class="old_page">Azclimart</a>
            </div>
            <div class="breadcrump_img">
              <img src="img/breadcrump.svg" alt="">
            </div>
            <span class="current_page">Əlaqə</span>
          </div>
          <p class="title_repeat">Əlaqə</p>
          <div class="contact_grid_box w-100">
            <div class="contact_left">
              <div class="contact_phone">
                <p class="title_con_left">Telefon</p>
                <div class="phones">
                  <p class="same_contact_text">+99451 206 53 66</p>
                  <p class="same_contact_text">+99451 206 53 66</p>
                </div>
              </div>
              <div class="contact_address">
                <p class="title_con_left">Ünvan</p>
                <div class="phones">
                  <p class="same_contact_text">Bakı ş. Nərimanov ray. Ziya</p>
                </div>
              </div>
              <div class="contact_address">
                <p class="title_con_left">E-mail</p>
                <div class="phones">
                  <p class="same_contact_text">info@katsangroup.az</p>
                </div>
              </div>
              <div class="contact_socials">
                <a href="#"><img src="img/fb_blue.svg" alt=""></a>
                <a href=""><img src="img/insta_blue.svg" alt=""></a>
              </div>
            </div>
            <div class="contact_right">
              <p class="write_us">Bizə yazın</p>
              <form action="" id="contact_form">
                <div class="form-group">
                  <input class="inputValidate" type="text" name="name" required>
                  <label class="place-label">Ad <span>*</span></label>
                </div>
                <div class="form-group">
                  <input class="inputValidate" type="text" name="father_name" required>
                  <label class="place-label">Soyad  Ata adı <span>*</span></label>
                </div>
                <div class="form-group">
                  <input class="inputValidate" type="email" name="email" required>
                  <label class="place-label">Email <span>*</span></label>
                </div>
                <div class="form-group">
                  <input class="inputValidate number_input" type="number" minlength="10" name="phone" required>
                  <label class="place-label">Telefon nömrəsi <span>*</span></label>
                </div>
                <div class="form-group textarea_group">
                  <textarea name="textarea"></textarea>
                  <label class="place-label">Əlavə qeyd </label>
                </div>
                <button type="submit" class="btn_blue">Göndər</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php
      include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>