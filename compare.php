<?php
    include("includes/head.php");
?>
<section class="compare">
    <?php
        include("includes/header.php");
    ?>
    <div class="compare_full_container">
      <div class="container">
        <div class="row">
          <div class="breadcrumps">
            <div class="page_main">
              <a href="index.php" class="old_page">Azclimart</a>
            </div>
            <div class="breadcrump_img">
              <img src="img/breadcrump.svg" alt="">
            </div>
            <span class="current_page">Müqayisə et</span>
          </div>
        </div>
      </div>
      <div class="compare_main_container">
        <p class="title_repeat">Müqayisə et</p>
        
        <div class="compared_boxes">
          <div class="compared_single_box">
            <div class="compared_left_info">
              <p class="com_left_title">Kondisioner</p>
              <div class="left_btns">
                <a href="projects.php" class="white-blue"><img src="img/plus_add.svg" alt=""> Məhsul əlavə et</a>
                <button class="btn_blue">Müqayisə et</button>
              </div>
            </div>
            <div class="swiper-container compare-swiper">
              <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <div class="swiper-same-box">
                    <button class="delete">
                        <img src="img/esc.svg" alt="">
                    </button>
                    <div class="item_standart">
                      <a href="#" class="item_img">
                        <div class="dis_type">-15%</div>
                        <img src="img/product.jpg" alt="">
                      </a>
                      <div class="item_info">
                        <div class="item_price_box">
                          <div class="prices">
                            <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                            <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                          </div>
                          <div class="discount_box">
                            <div class="month">12 ay</div>
                            <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                          </div>
                        </div>
                        <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                        <div class="item_controller">
                          <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                          <a href="#">
                            <img src="img/compare_gray.svg" class="item_gray" alt="">
                            <img src="img/compare_blue.svg" class="item_blue" alt="">
                          </a>
                          <a href="#">
                            <img src="img/fav_gray.svg" class="item_gray" alt="">
                            <img src="img/fav_blue.svg" class="item_blue" alt="">
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="swiper-same-box">
                    <button class="delete">
                        <img src="img/esc.svg" alt="">
                    </button>
                    <div class="item_standart">
                      <a href="#" class="item_img">
                        <div class="dis_type">-15%</div>
                        <img src="img/product.jpg" alt="">
                      </a>
                      <div class="item_info">
                        <div class="item_price_box">
                          <div class="prices">
                            <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                            <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                          </div>
                          <div class="discount_box">
                            <div class="month">12 ay</div>
                            <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                          </div>
                        </div>
                        <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                        <div class="item_controller">
                          <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                          <a href="#">
                            <img src="img/compare_gray.svg" class="item_gray" alt="">
                            <img src="img/compare_blue.svg" class="item_blue" alt="">
                          </a>
                          <a href="#">
                            <img src="img/fav_gray.svg" class="item_gray" alt="">
                            <img src="img/fav_blue.svg" class="item_blue" alt="">
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="swiper-same-box">
                    <button class="delete">
                        <img src="img/esc.svg" alt="">
                    </button>
                    <div class="item_standart">
                      <a href="#" class="item_img">
                        <div class="dis_type">-15%</div>
                        <img src="img/product.jpg" alt="">
                      </a>
                      <div class="item_info">
                        <div class="item_price_box">
                          <div class="prices">
                            <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                            <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                          </div>
                          <div class="discount_box">
                            <div class="month">12 ay</div>
                            <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                          </div>
                        </div>
                        <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                        <div class="item_controller">
                          <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                          <a href="#">
                            <img src="img/compare_gray.svg" class="item_gray" alt="">
                            <img src="img/compare_blue.svg" class="item_blue" alt="">
                          </a>
                          <a href="#">
                            <img src="img/fav_gray.svg" class="item_gray" alt="">
                            <img src="img/fav_blue.svg" class="item_blue" alt="">
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="swiper-same-box">
                    <button class="delete">
                        <img src="img/esc.svg" alt="">
                    </button>
                    <div class="item_standart">
                      <a href="#" class="item_img">
                        <div class="dis_type">-15%</div>
                        <img src="img/product.jpg" alt="">
                      </a>
                      <div class="item_info">
                        <div class="item_price_box">
                          <div class="prices">
                            <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                            <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                          </div>
                          <div class="discount_box">
                            <div class="month">12 ay</div>
                            <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                          </div>
                        </div>
                        <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                        <div class="item_controller">
                          <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                          <a href="#">
                            <img src="img/compare_gray.svg" class="item_gray" alt="">
                            <img src="img/compare_blue.svg" class="item_blue" alt="">
                          </a>
                          <a href="#">
                            <img src="img/fav_gray.svg" class="item_gray" alt="">
                            <img src="img/fav_blue.svg" class="item_blue" alt="">
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container container-single">
              <div class="row">
                <div class="compared_bottom w-100">
                  <div class="compared_table">
                    <div class="compared_row">
                      <p>Qiymət</p>
                      <p>bloklu</p>
                      <p>Multi sistem 1 bloklu</p>
                      <p>Multi sistem 1 bloklu</p>
                      <p>Multi sistem 1 bloklu</p>
                    </div>
                    <div class="compared_row">
                      <p>Brendlər</p>
                      <p>3600</p>
                      <p></p>
                      <p>3600</p>
                      <p>3600</p>
                    </div>
                    <div class="compared_row">
                      <p>Növ</p>
                      <p>5</p>
                      <p>5</p>
                      <p>5</p>
                      <p>5</p>
                    </div>
                    <div class="compared_row">
                      <p>İnverter</p>
                      <p></p>
                      <p>R32</p>
                      <p></p>
                      <p>R32</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="compared_single_box">
            <div class="compared_left_info">
              <p class="com_left_title">Kondisioner</p>
              <div class="left_btns">
                <a href="projects.php" class="white-blue"><img src="img/plus_add.svg" alt=""> Məhsul əlavə et</a>
                <button class="btn_blue">Müqayisə et</button>
              </div>
            </div>
            <div class="swiper-container compare-swiper">
              <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <div class="swiper-same-box">
                    <button class="delete">
                        <img src="img/esc.svg" alt="">
                    </button>
                    <div class="item_standart">
                      <a href="#" class="item_img">
                        <div class="dis_type">-15%</div>
                        <img src="img/product.jpg" alt="">
                      </a>
                      <div class="item_info">
                        <div class="item_price_box">
                          <div class="prices">
                            <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                            <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                          </div>
                          <div class="discount_box">
                            <div class="month">12 ay</div>
                            <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                          </div>
                        </div>
                        <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                        <div class="item_controller">
                          <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                          <a href="#">
                            <img src="img/compare_gray.svg" class="item_gray" alt="">
                            <img src="img/compare_blue.svg" class="item_blue" alt="">
                          </a>
                          <a href="#">
                            <img src="img/fav_gray.svg" class="item_gray" alt="">
                            <img src="img/fav_blue.svg" class="item_blue" alt="">
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="swiper-same-box">
                    <button class="delete">
                        <img src="img/esc.svg" alt="">
                    </button>
                    <div class="item_standart">
                      <a href="#" class="item_img">
                        <div class="dis_type">-15%</div>
                        <img src="img/product.jpg" alt="">
                      </a>
                      <div class="item_info">
                        <div class="item_price_box">
                          <div class="prices">
                            <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                            <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                          </div>
                          <div class="discount_box">
                            <div class="month">12 ay</div>
                            <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                          </div>
                        </div>
                        <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                        <div class="item_controller">
                          <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                          <a href="#">
                            <img src="img/compare_gray.svg" class="item_gray" alt="">
                            <img src="img/compare_blue.svg" class="item_blue" alt="">
                          </a>
                          <a href="#">
                            <img src="img/fav_gray.svg" class="item_gray" alt="">
                            <img src="img/fav_blue.svg" class="item_blue" alt="">
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="swiper-same-box">
                    <button class="delete">
                        <img src="img/esc.svg" alt="">
                    </button>
                    <div class="item_standart">
                      <a href="#" class="item_img">
                        <div class="dis_type">-15%</div>
                        <img src="img/product.jpg" alt="">
                      </a>
                      <div class="item_info">
                        <div class="item_price_box">
                          <div class="prices">
                            <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                            <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                          </div>
                          <div class="discount_box">
                            <div class="month">12 ay</div>
                            <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                          </div>
                        </div>
                        <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                        <div class="item_controller">
                          <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                          <a href="#">
                            <img src="img/compare_gray.svg" class="item_gray" alt="">
                            <img src="img/compare_blue.svg" class="item_blue" alt="">
                          </a>
                          <a href="#">
                            <img src="img/fav_gray.svg" class="item_gray" alt="">
                            <img src="img/fav_blue.svg" class="item_blue" alt="">
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="swiper-same-box">
                    <button class="delete">
                        <img src="img/esc.svg" alt="">
                    </button>
                    <div class="item_standart">
                      <a href="#" class="item_img">
                        <div class="dis_type">-15%</div>
                        <img src="img/product.jpg" alt="">
                      </a>
                      <div class="item_info">
                        <div class="item_price_box">
                          <div class="prices">
                            <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                            <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                          </div>
                          <div class="discount_box">
                            <div class="month">12 ay</div>
                            <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                          </div>
                        </div>
                        <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                        <div class="item_controller">
                          <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                          <a href="#">
                            <img src="img/compare_gray.svg" class="item_gray" alt="">
                            <img src="img/compare_blue.svg" class="item_blue" alt="">
                          </a>
                          <a href="#">
                            <img src="img/fav_gray.svg" class="item_gray" alt="">
                            <img src="img/fav_blue.svg" class="item_blue" alt="">
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container container-single">
              <div class="row">
                <div class="compared_bottom w-100">
                  <div class="compared_table">
                    <div class="compared_row">
                      <p>Qiymət</p>
                      <p>bloklu</p>
                      <p>Multi sistem 1 bloklu</p>
                      <p>Multi sistem 1 bloklu</p>
                      <p>Multi sistem 1 bloklu</p>
                    </div>
                    <div class="compared_row">
                      <p>Brendlər</p>
                      <p>3600</p>
                      <p></p>
                      <p>3600</p>
                      <p>3600</p>
                    </div>
                    <div class="compared_row">
                      <p>Növ</p>
                      <p>5</p>
                      <p>5</p>
                      <p>5</p>
                      <p>5</p>
                    </div>
                    <div class="compared_row">
                      <p>İnverter</p>
                      <p></p>
                      <p>R32</p>
                      <p></p>
                      <p>R32</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php
        include("includes/footer.php");
    ?>
</section>


<?php
    include("includes/script.php");
?>