<?php
    include("includes/head.php");
?>
<section class="basket">
    <?php
        include("includes/header.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="basket_container w-100">
          <div class="breadcrumps">
              <div class="page_main">
                <a href="index.php" class="old_page">Azclimart</a>
              </div>
              <div class="breadcrump_img">
                <img src="img/breadcrump.svg" alt="">
              </div>
              <div class="page_main">
                <a href="index.php" class="old_page">Məhsullar</a>
              </div>
              <div class="breadcrump_img">
                <img src="img/breadcrump.svg" alt="">
              </div>
              <div class="page_main">
                <a href="index.php" class="old_page">Kondisioner AUX Aswho9a4RR - 9000 BTU Kondisioner AUX Aswho9a4RR</a>
              </div>
              <div class="breadcrump_img">
                <img src="img/breadcrump.svg" alt="">
              </div>
              <span class="current_page">Səbət</span>
          </div>
          <p class="title_repeat">Səbət</p>
          <div class="basket_operation_container">
            <div class="basket_box">
              <div class="basket_same">
                <button class="delete">
                    <img src="img/esc.svg" alt="">
                </button>
                <div class="basket_single w-100">
                  <div class="basket_img">
                    <img src="img/basket1.png" alt="">
                  </div>
                  <div class="basket_content_box">
                    <div class="basket_content_title">
                      <p class="title_of_project">Kondisioner AUX Aswho9a4RR - 9000 BTU Aswho9a4RR</p>
                      <p class="type_of_project">Məhsulun kodu : <span>12343526181872</span></p>
                    </div>
                  </div>
                  <div class="basket_operation">
                    <div class="basket_count_box" data-target="amount-1">
                      <button class="cart-minus-1"><img src="img/minus.svg" alt=""></button>
                      <input type="number" id="amount-1" value="1" name="" min="1">
                      <button class="cart-plus-1"><img src="img/plus.svg" alt=""></button>
                    </div>
                    <p class="money_price" ><span data-price="576.4">576.4</span><img src="img/manat_basket.png" alt=""></p>
                  </div>
                </div>
              </div>
              <div class="basket_same">
                <button class="delete">
                    <img src="img/esc.svg" alt="">
                </button>
                <div class="basket_single w-100">
                  <div class="basket_img">
                    <img src="img/basket1.png" alt="">
                  </div>
                  <div class="basket_content_box">
                    <div class="basket_content_title">
                      <p class="title_of_project">Kondisioner AUX Aswho9a4RR - 9000 BTU Aswho9a4RR</p>
                      <p class="type_of_project">Məhsulun kodu : <span>12343526181872</span></p>
                    </div>
                  </div>
                  <div class="basket_operation">
                    <div class="basket_count_box" data-target="amount-2">
                      <button class="cart-minus-1"><img src="img/minus.svg" alt=""></button>
                      <input type="number" id="amount-2" value="1" name="" min="1">
                      <button class="cart-plus-1"><img src="img/plus.svg" alt=""></button>
                    </div>
                    <p class="money_price" ><span data-price="576.4">576.4</span><img src="img/manat_basket.png" alt=""></p>
                  </div>
                </div>
              </div>
              <div class="basket_same">
                <button class="delete">
                    <img src="img/esc.svg" alt="">
                </button>
                <div class="basket_single w-100">
                  <div class="basket_img">
                    <img src="img/basket1.png" alt="">
                  </div>
                  <div class="basket_content_box">
                    <div class="basket_content_title">
                      <p class="title_of_project">Kondisioner AUX Aswho9a4RR - 9000 BTU Aswho9a4RR</p>
                      <p class="type_of_project">Məhsulun kodu : <span>12343526181872</span></p>
                    </div>
                  </div>
                  <div class="basket_operation">
                    <div class="basket_count_box" data-target="amount-3">
                      <button class="cart-minus-1"><img src="img/minus.svg" alt=""></button>
                      <input type="number" id="amount-3" value="1" name="" min="1">
                      <button class="cart-plus-1"><img src="img/plus.svg" alt=""></button>
                    </div>
                    <p class="money_price" ><span data-price="576.4">576.4</span><img src="img/manat_basket.png" alt=""></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="result_box">
              <div class="title_total_box">
                <div class="heading">Ümumi məbləğ</div>
                <p>
                  <span id="total">12124</span><img src="img/total_basket.png" alt="">
                </p>
              </div>
              <div class="basket_btns">
                <a href="pay.php" class="pay_btn blue-white">Ödənişi tamamla</a>
                <a href="more_items.php" class="back_items white-blue">Alışverişə geri qayıt</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
