<?php
    include("includes/head.php");
?>

<div id="loading"><img src="img/ball.gif" alt=""></div>

<section class="projects">
    <?php
        include("includes/header.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="projects_container">

          <div class="breadcrumps">
            <div class="page_main">
              <a href="index.php" class="old_page">Azclimart</a>
            </div>
            <div class="breadcrump_img">
              <img src="img/breadcrump.svg" alt="">
            </div>
            <span class="current_page">Məhsullar</span>
          </div>

          <div class="projects_main_box">
              <div></div>

               <div class="project_heading_container">
                  <p class="title_repeat">Məhsullar</p>
                  <ul id="online">
                    <li><button data-tab='1' class="active_link">Kondisioner</button></li>
                    <li><button data-tab='2'>Havalandırma</button></li>
                    <li><button data-tab='3'>Santexnika</button></li>
                  </ul>
                  <button class="filter">
                    <p>Filter</p>
                    <img src="img/filter.svg" alt="">
                  </button>
               </div>

              <aside class="aside-form">
                <button class="esc"><img src="img/esc.svg" alt=""></button>
                <form action="" class="left_filter_box">
                  <div class="filter_header">
                    <p class="filter_title">Filter</p>
                    <button id="clean">Təmizlə</button>
                  </div>
                  <div class="filter_operation">
                    <div class="filter_special_box">
                      <div class="f_spe_heading">
                        <p>Qiymət</p>
                      </div>
                       <div class="block range-block">
                        <input type="text" class="js-range-slider" name="my_range" value="" />
                       </div>     
                    </div>
                    <div class="filter_special_box">
                        <div class="f_spe_heading">
                          <p>Brendlər</p>
                        </div>
                        <div class="block">
                           <div class="brand_links">
                             <a href="#">Kas</a>
                             <a href="#">Ostendorf</a>
                             <a href="#">Samsung</a>
                             <a href="#">Mitsibushi</a>
                             <a href="#">Ostendorf</a>
                             <a href="#">Kas</a>
                             <a href="#">Samsung</a>
                             <a href="#">Ostendorf</a>
                             <a href="#">Mitsibushi</a>
                             <a href="#">Samsung</a>
                             <a href="#">Kas</a>
                           </div>
                       </div>
                    </div>
                    <div class="filter_special_box">
                        <div class="f_spe_heading">
                          <p>Növ</p>
                        </div>
                        <div class="block">
                          <div class="label_box">
                            <div class="label_single">
                              <input type="checkbox" name="split" id="split">
                              <label for="split" class="label_project">Split sistemi</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="portativ" id="portativ">
                              <label for="portativ" class="label_project">Portativ</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="kolon" id="kolon">
                              <label for="kolon" class="label_project">Kolon tipli</label>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="filter_special_box">
                        <div class="f_spe_heading">
                          <p>Təsir sahəsi</p>
                        </div>
                        <div class="block">
                          <div class="label_box">
                            <div class="label_single">
                              <input type="checkbox" name="20" id="20">
                              <label for="20" class="label_project">20-30 kv</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="25_35" id="25_35">
                              <label for="25_35" class="label_project">25-35</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="25" id="25">
                              <label for="25" class="label_project">25</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="30_40" id="30_40">
                              <label for="30_40" class="label_project">30-40</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="50_60" id="50_60">
                              <label for="50_60" class="label_project">50-60</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="65_70" id="65_70">
                              <label for="65_70" class="label_project">65-70</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="70" id="70">
                              <label for="70" class="label_project">70</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="70_80" id="70_80">
                              <label for="70_80" class="label_project">70-80</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="75_80" id="75_80">
                              <label for="75_80" class="label_project">75-80</label>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="filter_special_box">
                        <div class="f_spe_heading">
                          <p>İnverter</p>
                        </div>
                        <div class="block">
                          <div class="label_box">
                            <div class="label_single">
                              <input type="checkbox" name="yes" id="yes">
                              <label for="yes" class="label_project">Var</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="no" id="no">
                              <label for="no" class="label_project">Yox</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="inverter" id="inverter">
                              <label for="inverter" class="label_project">İnverter</label>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="filter_special_box">
                        <div class="f_spe_heading">
                          <p>Enerjistifadə sinfi</p>
                        </div>
                        <div class="block">
                          <div class="label_box">
                            <div class="label_single">
                              <input type="checkbox" name="a" id="a">
                              <label for="a" class="label_project">A</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="b" id="b">
                              <label for="b" class="label_project">B</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="c" id="c">
                              <label for="c" class="label_project">C</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="d" id="d">
                              <label for="d" class="label_project">D</label>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="filter_special_box">
                        <div class="f_spe_heading">
                          <p>Rəng</p>
                        </div>
                        <div class="block">
                          <div class="label_box">
                            <div class="label_single">
                              <input type="checkbox" name="white" id="white">
                              <label for="white" class="label_project">Ağ</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="mirror" id="mirror">
                              <label for="mirror" class="label_project">Mirror</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="black" id="black">
                              <label for="black" class="label_project">Qara</label>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="filter_special_box">
                        <div class="f_spe_heading">
                          <p>Məhsuldarlıq</p>
                        </div>
                        <div class="block">
                          <div class="label_box">
                            <div class="label_single">
                              <input type="checkbox" name="9k" id="9k">
                              <label for="9k" class="label_project">9000 BTU</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="10k" id="10k">
                              <label for="10k" class="label_project">10000 BTU</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="11k" id="11k">
                              <label for="11k" class="label_project">11000 BTU</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="12k" id="12k">
                              <label for="12k" class="label_project">12000 BTU</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="13k" id="13k">
                              <label for="13k" class="label_project">13000 BTU</label>
                            </div>
                            <div class="label_single">
                              <input type="checkbox" name="14k" id="14k">
                              <label for="14k" class="label_project">14000 BTU</label>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                </form>
              </aside>


              <div class="project_filtered_boxes">
                <div class="item_standart" data-id='1'>
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item_standart" data-id='2'>
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item_standart" data-id='3'>
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item_standart" data-id='1'>
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item_standart" data-id='2'>
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item_standart" data-id='3'>
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item_standart" data-id='1'>
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item_standart" data-id='2'>
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item_standart" data-id='3'>
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item_standart" data-id='1'>
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item_standart" data-id='2'>
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item_standart" data-id='3'>
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item_standart" data-id='1'>
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <div class="item_standart">
                  <a href="#" class="item_img">
                    <div class="dis_type">-15%</div>
                    <img src="img/product.jpg" alt="">
                  </a>
                  <div class="item_info">
                    <div class="item_price_box">
                      <div class="prices">
                        <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                        <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                      </div>
                    </div>
                    <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                    <div class="item_controller">
                      <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                      <a href="#">
                        <img src="img/compare_gray.svg" class="item_gray" alt="">
                        <img src="img/compare_blue.svg" class="item_blue" alt="">
                      </a>
                      <a href="#">
                        <img src="img/fav_gray.svg" class="item_gray" alt="">
                        <img src="img/fav_blue.svg" class="item_blue" alt="">
                      </a>
                    </div>
                  </div>
                </div>
                <button class="see_more">Daha çox gör</button>
              </div>
          </div>

          <div class="box_best_new">
            <div class="heading_best">
              <p class="title_repeat">Ən çox satan</p>
              <a href="#">Daha çox</a>
            </div>
            <div class="container_grid_4">
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                    <div class="discount_box">
                      <div class="month">12 ay</div>
                      <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                    <div class="discount_box">
                      <div class="month">12 ay</div>
                      <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                    <div class="discount_box">
                      <div class="month">12 ay</div>
                      <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
              <div class="item_standart">
                <a href="#" class="item_img">
                  <div class="dis_type">-15%</div>
                  <img src="img/product.jpg" alt="">
                </a>
                <div class="item_info">
                  <div class="item_price_box">
                    <div class="prices">
                      <div class="current_price">250 <img src="img/manat.svg" alt=""></div>
                      <div class="old_price">350 <img src="img/manat.svg" alt=""></div>
                    </div>
                    <div class="discount_box">
                      <div class="month">12 ay</div>
                      <div class="dis_price">60 <img src="img/manat.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="item_content"><p>Kondisioner AUX Aswho9a4RR - 9000 BTU AUX Aswho9a4RR</p></div>
                  <div class="item_controller">
                    <a href="basket.php" class="to_basket"><img src="img/basket_blue.svg" alt=""><span>Səbətə at</span></a>
                    <a href="#">
                      <img src="img/compare_gray.svg" class="item_gray" alt="">
                      <img src="img/compare_blue.svg" class="item_blue" alt="">
                    </a>
                    <a href="#">
                      <img src="img/fav_gray.svg" class="item_gray" alt="">
                      <img src="img/fav_blue.svg" class="item_blue" alt="">
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
