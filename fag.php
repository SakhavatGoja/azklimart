<?php
    include("includes/head.php");
?>
<section class="fag">
    <?php
        include("includes/header.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="fag_container w-100">
          <div class="breadcrumps">
              <div class="page_main">
                <a href="index.php" class="old_page">Azclimart</a>
              </div>
              <div class="breadcrump_img">
                <img src="img/breadcrump.svg" alt="">
              </div>
              <span class="current_page">Məhsullar</span>
          </div>
          <p class="title_repeat">FAG</p>
          <p class="fag_desc">
              Bu bölmədə bizə tez-tez ünvanlanan sualların cavabını tapa bilərsiniz.
              Əgər sizin sualınız siyahıda yoxdursa, o zaman sualınızı bizə ünvanlayın, ən qısa zaman ərzində cavablayaq.
          </p>
          <div class="fag_accordion">
            <ul id="accordion" class="accordion">
              <li>
                <div class="link">
                    <p class="link_accordion">Lorem ipsum dolor sit amet, consectetur adipiscing elit consectetur?</p>
                    <button><img src="img/accordion.svg" alt=""></button>
                </div>
                <div class="submenu">
                    <p class="submenu_inner">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mi mi fringilla ut sit blandit lacus.
                      Integer id sit condimentum enim urna non neque, tempor. Facilisis quis semper vitae dictum cursus lacus.
                      Suscipit morbi id aliquam nibh et sit. Gravida nisi, volutpat ac, consectetur ipsum ultricies eu magna.
                        Suscipit morbi id aliquam nibh et sit. Gravida nisi, volutpat ac, consectetur ipsum ultricies eu magna. 
                    </p>
                </div>
              </li>
              <li>
                <div class="link">
                    <p class="link_accordion">Lorem ipsum dolor sit amet, consectetur adipiscing elit consectetur?</p>
                    <button><img src="img/accordion.svg" alt=""></button>
                </div>
                <div class="submenu">
                    <p class="submenu_inner">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mi mi fringilla ut sit blandit lacus.
                      Integer id sit condimentum enim urna non neque, tempor. Facilisis quis semper vitae dictum cursus lacus.
                      Suscipit morbi id aliquam nibh et sit. Gravida nisi, volutpat ac, consectetur ipsum ultricies eu magna.
                        Suscipit morbi id aliquam nibh et sit. Gravida nisi, volutpat ac, consectetur ipsum ultricies eu magna. 
                    </p>
                </div>
              </li>
              <li>
                <div class="link">
                    <p class="link_accordion">Lorem ipsum dolor sit amet, consectetur adipiscing elit consectetur?</p>
                    <button><img src="img/accordion.svg" alt=""></button>
                </div>
                <div class="submenu">
                    <p class="submenu_inner">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mi mi fringilla ut sit blandit lacus.
                      Integer id sit condimentum enim urna non neque, tempor. Facilisis quis semper vitae dictum cursus lacus.
                      Suscipit morbi id aliquam nibh et sit. Gravida nisi, volutpat ac, consectetur ipsum ultricies eu magna.
                        Suscipit morbi id aliquam nibh et sit. Gravida nisi, volutpat ac, consectetur ipsum ultricies eu magna. 
                    </p>
                </div>
              </li>
              <li>
                <div class="link">
                    <p class="link_accordion">Lorem ipsum dolor sit amet, consectetur adipiscing elit consectetur?</p>
                    <button><img src="img/accordion.svg" alt=""></button>
                </div>
                <div class="submenu">
                    <p class="submenu_inner">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mi mi fringilla ut sit blandit lacus.
                      Integer id sit condimentum enim urna non neque, tempor. Facilisis quis semper vitae dictum cursus lacus.
                      Suscipit morbi id aliquam nibh et sit. Gravida nisi, volutpat ac, consectetur ipsum ultricies eu magna.
                        Suscipit morbi id aliquam nibh et sit. Gravida nisi, volutpat ac, consectetur ipsum ultricies eu magna. 
                    </p>
                </div>
              </li>
              <li>
                <div class="link">
                    <p class="link_accordion">Lorem ipsum dolor sit amet, consectetur adipiscing elit consectetur?</p>
                    <button><img src="img/accordion.svg" alt=""></button>
                </div>
                <div class="submenu">
                    <p class="submenu_inner">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mi mi fringilla ut sit blandit lacus.
                      Integer id sit condimentum enim urna non neque, tempor. Facilisis quis semper vitae dictum cursus lacus.
                      Suscipit morbi id aliquam nibh et sit. Gravida nisi, volutpat ac, consectetur ipsum ultricies eu magna.
                        Suscipit morbi id aliquam nibh et sit. Gravida nisi, volutpat ac, consectetur ipsum ultricies eu magna. 
                    </p>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>